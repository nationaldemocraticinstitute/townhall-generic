<?php
/**
 * @file
 * th_admin_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function th_admin_roles_user_default_roles() {
  $roles = array();

  // Exported role: Content manager.
  $roles['Content manager'] = array(
    'name' => 'Content manager',
    'weight' => 12,
  );

  // Exported role: Site editor.
  $roles['Site editor'] = array(
    'name' => 'Site editor',
    'weight' => 11,
  );

  return $roles;
}
